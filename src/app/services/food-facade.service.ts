import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';

import { foodUrl } from '../base-url';
import { Food, FoodState, Pagination, Res } from '../models/facade';
import { Result } from '../models/result';
import { _searchFilter } from '../util/function-lib';

// tslint:disable-next-line: variable-name
let _state: FoodState = {
  foods: [],
  criteria: '',
  pagination: {
    currentPage: 0,
    // selectedSize: 5,
    pageSizes: 5
  },
  response: {
    status: 0,
    msg: ''
  },
  loading: false
};

@Injectable({
  providedIn: 'root'
})
export class FoodFacadeService {
  private store = new BehaviorSubject<FoodState>(_state);
  private state$ = this.store.asObservable();

  foods$ = this.state$.pipe(
    map(state => state.foods),
    distinctUntilChanged()
  );
  criteria$ = this.state$.pipe(
    map(state => state.criteria),
    distinctUntilChanged()
  );
  pagination$ = this.state$.pipe(
    map(state => state.pagination),
    distinctUntilChanged()
  );
  response$ = this.state$.pipe(
    map(status => status.response),
    distinctUntilChanged()
  );
  loading$ = this.state$.pipe(
    map(state => state.loading),
    distinctUntilChanged()
  );

  vm$: Observable<FoodState> = combineLatest([
    this.pagination$,
    this.criteria$,
    this.foods$,
    this.response$,
    this.loading$
  ]).pipe(
    map(([pagination, criteria, foods, response, loading]) => {
      return { pagination, criteria, foods, response, loading };
    })
  );

  constructor(private http: HttpClient) {
    // combineLatest([this.criteria$, this.pagination$])
    this.loading$
      .pipe(
        switchMap(() => {
          return this.findAllFoods();
        })
      )
      .subscribe(foods => {
        _state = { ..._state, foods, loading: false };
        this.updateState(_state);
      });
  }

  // ------- Public Methods ------------------------

  // Allows quick snapshot access to data for ngOnInit() purposes
  getStateSnapshot(): FoodState {
    return { ..._state };
  }

  buildSearchTermControl(): FormControl {
    const searchTerm = new FormControl();
    searchTerm.valueChanges
      .pipe(debounceTime(300), distinctUntilChanged())
      .subscribe(value => this.updateSearchCriteria(value));
    return searchTerm;
  }

  updateSearchCriteria(criteria: string) {
    this.updateState((_state = { ..._state, criteria }));
  }

  updateLoading(loading: boolean) {
    this.updateState({ ..._state, loading });
  }

  post(food: Food) {
    return this.postFood(food);
  }

  put(food: Food) {
    this.postFood(food).subscribe(res => {
      const response: Res = { status: res.status, msg: res.msg };
      let loading = false;
      if (res.status === 0) {
        loading = true;
      }
      _state = { ..._state, loading, response };
    });
  }

  // updatePagination(selectedSize: number, currentPage: number = 0) {
  //   const pagination = { ..._state.pagination, currentPage, selectedSize };
  //   this.updateState({ ..._state, pagination, loading: true });
  // }

  // ------- Private Methods ------------------------

  /** Update internal state cache and emit from store... */
  private updateState(state: FoodState) {
    const foods = state.foods.filter(food =>
      _searchFilter(state.criteria, food, ['name', 'type'], 0)
    );
    this.store.next({ ...state, foods });
  }

  /** RandomUser REST call */
  // private findAllFoods(
  //   criteria: string,
  //   pagination: Pagination
  // ): Observable<Food[]> {
  //   const url = buildUserUrl(criteria, pagination);
  //   return this.http.get<Result<Food[]>>(url).pipe(
  //     map(response => {
  //       return response.data;
  //     })
  //   );
  // }
  private findAllFoods(): Observable<Food[]> {
    return this.http.get<Result<Food[]>>(foodUrl).pipe(
      map(response => {
        return response.data;
      })
    );
  }

  private postFood(food: Food): Observable<Result<Food>> {
    return this.http.post<Result<Food>>(foodUrl, food).pipe();
  }

  private updateFood(food: Food): Observable<Result<Food>> {
    return this.http
      .put<Result<Food>>(foodUrl + `${food.id}` + '/', food)
      .pipe();
  }
}

function buildUserUrl(criteria: string, pagination: Pagination): string {
  const URL = foodUrl;
  const currentPage = `page=${pagination.currentPage}`;
  const pageSize = `results=${pagination.pageSizes}&`;
  const searchFor = `seed=${criteria}`;

  // return `${URL}?${searchFor}&${pageSize}&${currentPage}`;
  return `${URL}`;
}

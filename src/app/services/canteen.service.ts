import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { tzDetailUrl, tzUrl } from '../base-url';
import { Result } from '../models/result';

@Injectable({
  providedIn: 'root'
})
export class CanteenService {
  constructor(private http: HttpClient) {}

  tzPost(tz) {
    return this.http.post<Result<any>>(tzUrl, tz);
  }
  // 更新unit接口
  tzPut(tz) {
    const id = tz.id;
    return this.http.put<Result<any>>(tzUrl + id + '/', tz);
  }
  // 删除unit接口
  tzDelete(id: string) {
    return this.http.delete<Result<any>>(tzUrl + id + '/');
  }
  // 查询unit接口
  tzGet(): Observable<any[]> {
    return this.http.get<Result<any[]>>(tzUrl).pipe(
      map(res => {
        if (res.status === 0) {
          return res.data;
        } else {
          return [];
        }
      })
    );
  }

  tzDetailPost(tzDetails) {
    return this.http.post<Result<any>>(tzDetailUrl, tzDetails);
  }
  // 更新unit接口
  tzDetailPut(tzDetails) {
    const id = tzDetails.id;
    return this.http.put<Result<any>>(tzDetailUrl + id + '/', tzDetails);
  }
  // 删除unit接口
  tzDetailDelete(id: string) {
    return this.http.delete<Result<any>>(tzDetailUrl + id + '/');
  }
  // 查询unit接口
  tzDetailGet(): Observable<any[]> {
    return this.http.get<Result<any[]>>(tzDetailUrl).pipe(
      map(res => {
        if (res.status === 0) {
          return res.data;
        } else {
          return [];
        }
      })
    );
  }
}

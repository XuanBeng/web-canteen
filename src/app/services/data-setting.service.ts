import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { foodUrl, seatUrl, userUrl } from '../base-url';
import { Result } from '../models/result';

@Injectable({
  providedIn: 'root'
})
export class DataSettingService {
  constructor(private http: HttpClient) {}

  foodPost(food) {
    return this.http.post<Result<any>>(foodUrl, food);
  }
  // 更新unit接口
  foodPut(food) {
    const id = food.id;
    return this.http.put<Result<any>>(foodUrl + id + '/', food);
  }
  // 删除unit接口
  foodDelete(id: string) {
    return this.http.delete<Result<any>>(foodUrl + id + '/');
  }
  // 查询unit接口
  foodGet(): Observable<any[]> {
    return this.http.get<Result<any[]>>(foodUrl).pipe(
      map(res => {
        if (res.status === 0) {
          return res.data;
        } else {
          return [];
        }
      })
    );
  }

  seatPost(seat) {
    return this.http.post<Result<any>>(seatUrl, seat);
  }
  // 更新unit接口
  seatPut(seat) {
    const id = seat.id;
    return this.http.put<Result<any>>(seatUrl + id + '/', seat);
  }
  // 删除unit接口
  seatDelete(id: string) {
    return this.http.delete<Result<any>>(seatUrl + id + '/');
  }
  // 查询unit接口
  seatGet(): Observable<any[]> {
    return this.http.get<Result<any[]>>(seatUrl).pipe(
      map(res => {
        if (res.status === 0) {
          return res.data;
        } else {
          return [];
        }
      })
    );
  }

  userGet() {
    return this.http.get<Result<any[]>>(userUrl).pipe(
      map(res => {
        if (res.status === 0) {
          return res.data;
        } else {
          return [];
        }
      })
    );
  }
}

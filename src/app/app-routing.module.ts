import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './pages/login/login.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  {
    path: 'shell',
    loadChildren: () =>
      import('./pages/shell/shell.module').then(m => m.ShellModule)
  },
  {
    path: 'test',
    loadChildren: () =>
      import('./pages/test/test.module').then(m => m.TestModule)
  },
  {
    path: 'form',
    loadChildren: () =>
      import('./pages/joi-form/joi-form.module').then(m => m.JoiFormModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

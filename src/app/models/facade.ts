export interface Food {
  id?: number;
  type: string;
  name: string;
  price: number;
}

export interface Pagination {
  // selectedSize: number;
  currentPage: number;
  pageSizes: number;
}

export interface RandomUserResponse {
  results: Food[];
}

export interface Res {
  status: number;
  msg: string;
}

export interface FoodState {
  foods: Food[];
  response: Res;
  pagination: Pagination;
  criteria: string;
  loading: boolean;
}

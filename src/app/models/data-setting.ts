export interface Unit {
  id?: string;
  name?: string;
  grade?: string;
  parentId?: string;
  edit?: boolean;
}

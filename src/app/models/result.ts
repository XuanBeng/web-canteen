export interface Result<T> {
  status: number;
  msg: string;
  data: T;
}

export interface Content {
  id?: number;
  content?: any;
  date?: Date;
}

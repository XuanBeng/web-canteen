import { Unit } from './data-setting';

export interface User {
  finish?: boolean;
  unit_id?: string;
  typeName?: string;
  edit?: boolean;
  id?: string;
  username?: string;
  name?: string;
  password?: string;
  student_no?: string;
  mobile?: string;
  type?: string;
  unit?: Unit;
}

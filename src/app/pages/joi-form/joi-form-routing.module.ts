import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { JoiFormComponent } from './joi-form.component';

const routes: Routes = [
  {
    path: '',
    component: JoiFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JoiFormRoutingModule {}

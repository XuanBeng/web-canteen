import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { JoiFormRoutingModule } from './joi-form-routing.module';
import { JoiFormComponent } from './joi-form.component';

@NgModule({
  declarations: [JoiFormComponent],
  imports: [CommonModule, ReactiveFormsModule, JoiFormRoutingModule]
})
export class JoiFormModule {}

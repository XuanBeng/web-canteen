import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NzButtonModule, NzInputModule } from 'ng-zorro-antd';

import { TestRoutingModule } from './test-routing.module';
import { TestComponent } from './test.component';

@NgModule({
  declarations: [TestComponent],
  imports: [
    CommonModule,
    FormsModule,
    DragDropModule,
    NzButtonModule,
    NzInputModule,
    TestRoutingModule
  ],
  exports: [TestComponent]
})
export class TestModule {}

export interface Cell {
  data: Data;
  name: string;
}

export interface Data {
  if40: boolean;
  if20: boolean;
  if_dj: boolean;
}

export const cells: Cell[] = [
  {
    data: {
      if40: false,
      if20: false,
      if_dj: false
    },
    name: '007H0102'
  },
  {
    data: {
      if40: false,
      if20: true,
      if_dj: false
    },
    name: '007H0104'
  },
  {
    data: {
      if40: false,
      if20: false,
      if_dj: false
    },
    name: '007H0106'
  },
  {
    data: {
      if40: false,
      if20: false,
      if_dj: false
    },
    name: '007H0108'
  },
  {
    data: {
      if40: true,
      if20: false,
      if_dj: false
    },
    name: '007H0202'
  },
  {
    data: {
      if40: false,
      if20: false,
      if_dj: false
    },
    name: '007H0204'
  },
  {
    data: {
      if40: true,
      if20: false,
      if_dj: false
    },
    name: '007H0206'
  },
  {
    data: {
      if40: false,
      if20: true,
      if_dj: false
    },
    name: '007H0208'
  },
  {
    data: {
      if40: false,
      if20: false,
      if_dj: false
    },
    name: '007H0304'
  },
  {
    data: {
      if40: true,
      if20: false,
      if_dj: false
    },
    name: '007H0306'
  },
  {
    data: {
      if40: false,
      if20: false,
      if_dj: false
    },
    name: '007H0308'
  },
  {
    data: {
      if40: false,
      if20: false,
      if_dj: false
    },
    name: '007H0404'
  },
  {
    data: {
      if40: false,
      if20: true,
      if_dj: false
    },
    name: '007H0406'
  },
  {
    data: {
      if40: false,
      if20: false,
      if_dj: false
    },
    name: '007H0408'
  },
  {
    data: {
      if40: false,
      if20: false,
      if_dj: false
    },
    name: '007H0508'
  },
  {
    data: {
      if40: true,
      if20: false,
      if_dj: false
    },
    name: '007H0608'
  },
  {
    data: {
      if40: false,
      if20: false,
      if_dj: true
    },
    name: '007H0506'
  },
  {
    data: {
      if40: false,
      if20: true,
      if_dj: true
    },
    name: '007H0606'
  }
];

export const cells2: Cell[] = [
  {
    data: {
      if40: false,
      if20: false,
      if_dj: false
    },
    name: '007H0102'
  },
  {
    data: {
      if40: false,
      if20: true,
      if_dj: false
    },
    name: '007H0104'
  },
  {
    data: {
      if40: false,
      if20: false,
      if_dj: false
    },
    name: '007H0106'
  },
  {
    data: {
      if40: false,
      if20: false,
      if_dj: false
    },
    name: '007H0108'
  },
  {
    data: {
      if40: true,
      if20: false,
      if_dj: false
    },
    name: '007H0202'
  },
  {
    data: {
      if40: false,
      if20: false,
      if_dj: false
    },
    name: '007H0204'
  },
  {
    data: {
      if40: true,
      if20: false,
      if_dj: false
    },
    name: '007H0206'
  },
  {
    data: {
      if40: false,
      if20: true,
      if_dj: false
    },
    name: '007H0208'
  },
  {
    data: {
      if40: false,
      if20: false,
      if_dj: false
    },
    name: '007H0304'
  },
  {
    data: {
      if40: true,
      if20: false,
      if_dj: false
    },
    name: '007H0306'
  },
  {
    data: {
      if40: false,
      if20: false,
      if_dj: false
    },
    name: '007H0308'
  },
  {
    data: {
      if40: false,
      if20: false,
      if_dj: false
    },
    name: '007H0404'
  },
  {
    data: {
      if40: false,
      if20: true,
      if_dj: false
    },
    name: '007H0406'
  },
  {
    data: {
      if40: false,
      if20: false,
      if_dj: false
    },
    name: '007H0408'
  },
  {
    data: {
      if40: false,
      if20: false,
      if_dj: false
    },
    name: '007H0508'
  },
  {
    data: {
      if40: true,
      if20: false,
      if_dj: false
    },
    name: '007H0608'
  },
  {
    data: {
      if40: false,
      if20: false,
      if_dj: true
    },
    name: '007H0506'
  },
  {
    data: {
      if40: false,
      if20: true,
      if_dj: true
    },
    name: '007H0606'
  },
  {
    data: {
      if40: false,
      if20: false,
      if_dj: false
    },
    name: '007H0002'
  },
  {
    data: {
      if40: false,
      if20: false,
      if_dj: false
    },
    name: '007H0004'
  },
  {
    data: {
      if40: false,
      if20: false,
      if_dj: false
    },
    name: '007H0006'
  },
  {
    data: {
      if40: false,
      if20: false,
      if_dj: false
    },
    name: '007H0008'
  }
];

export const colorScheme = ['#FF5454', '#FD7F01', '#01FD7F'];

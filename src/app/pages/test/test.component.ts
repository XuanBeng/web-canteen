import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { map, shareReplay, withLatestFrom, debounceTime } from 'rxjs/operators';

import { Cell, cells, cells2 } from './cells';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.less']
})
export class TestComponent implements OnInit {
  box = {
    width: 60,
    height: 60
  };
  container = {
    width: 800,
    height: 600
  };
  colorDefault = {
    ctn20Color: '#FF5454',
    ctn40Color: '#FD7F01',
    stroke_color: '#01C4FD'
  };
  spaceWidth = 30;
  colorSubject = new BehaviorSubject<{
    ctn20Color: string;
    ctn40Color: string;
    stroke_color: string;
  }>(this.colorDefault);
  color$ = this.colorSubject.asObservable();

  cellsSubject = new BehaviorSubject<Cell[]>(cells);
  // cells$ = this.cellsSubject.asObservable().pipe(
  //   tap(celllist => {
  //     const bayName = celllist[0].name.substring(0, 4);
  //     this.bayNameSubject.next(bayName);
  //   }),
  //   shareReplay()
  // );
  boxSubject = new BehaviorSubject(this.box);
  bayName$: Observable<string>;
  svg$: Observable<{ width: number; height: number }>;
  cols$: Observable<any[]>;
  rows$: Observable<any[]>;
  data$: Observable<any[]>;

  constructor() {}

  ngOnInit(): void {
    this.bayName$ = this.cellsSubject.pipe(
      map(cellslist => {
        return cellslist[0].name.substring(0, 4);
      })
    );

    this.rows$ = combineLatest([this.cellsSubject, this.boxSubject]).pipe(
      map(([celllist, box]) => {
        const list = [];
        celllist.forEach(cell => {
          list.push(cell.name.substring(6, 8));
        });
        const newlist = Array.from(new Set(list)).sort((a, b) => +b - +a);
        const newObjList = newlist.map((row, i) => {
          return {
            x: 0,
            y: (i + 1) * box.height,
            dx: box.width / 2,
            dy: box.height / 2,
            text: row
          };
        });
        return newObjList;
      }),
      shareReplay()
    );

    this.cols$ = combineLatest([this.cellsSubject, this.boxSubject]).pipe(
      map(([celllist, box]) => {
        const list = [];
        celllist.forEach(cell => {
          list.push(cell.name.substring(4, 6));
        });
        const newlist = Array.from(new Set(list)).sort((a, b) => +a - +b);
        const origin =
          this.spaceWidth + (newlist.length * box.width) / 2 + box.width;

        const newObjList = newlist.map((col, i) => {
          let x = 0;
          if (newlist.includes('00')) {
            x =
              +col % 2 === 0
                ? origin - ((i + 1) / 2) * box.width
                : origin + (i / 2) * box.width;
          } else {
            x =
              +col % 2 === 0
                ? origin - (i / 2) * box.width
                : origin + ((i + 1) / 2) * box.width;
          }
          return {
            x,
            y: 0,
            dx: box.width / 2,
            dy: box.height / 2,
            text: col
          };
        });
        return newObjList;
      }),
      shareReplay()
    );

    this.svg$ = combineLatest([this.cols$, this.rows$, this.boxSubject]).pipe(
      map(([cols, rows, box]) => {
        const container = {
          width: (cols.length + 2) * box.width + this.spaceWidth,
          height: (rows.length + 1) * box.height
        };
        return container;
      })
    );

    this.data$ = combineLatest([
      this.cellsSubject,
      this.boxSubject,
      this.colorSubject
    ]).pipe(
      withLatestFrom(this.cols$, this.rows$),
      map(([[celllist, box, color], cols, rows]) => {
        const newCells = celllist.map(cell => {
          const colObj = cols.find(d => d.text === cell.name.substring(4, 6));
          const rowObj = rows.find(d => d.text === cell.name.substring(6, 8));
          let fill = 'white';
          if (cell.data.if20) {
            fill = color.ctn20Color;
          } else if (cell.data.if40) {
            fill = color.ctn40Color;
          }
          const cellObj = {
            width: box.width,
            height: box.height,
            x: colObj.x,
            y: rowObj.y,
            dx: box.width / 4,
            dy: box.height / 2,
            text: colObj.text + rowObj.text,
            stroke_width: cell.data.if_dj ? 2 : 1,
            stroke_color: cell.data.if_dj ? color.stroke_color : 'black',
            fill
          };
          return cellObj;
        });
        return newCells;
      })
    );
  }

  click(d: any) {
    window.alert('hello world' + d.text);
  }
  changeData() {
    this.cellsSubject.next(cells2);
  }

  changeData00() {
    this.cellsSubject.next(cells);
  }

  boxChangeWidth(width: number) {
    if (width <= 60) {
      width = 60;
    }
    this.box.width = +width;
    this.boxSubject.next(this.box);
  }

  boxChangeHeight(height: number) {
    if (height <= 60) {
      height = 60;
    }
    this.box.height = +height;
    this.boxSubject.next(this.box);
  }

  ctn20ColorChange(color: string) {
    this.colorDefault.ctn20Color = color;
    this.colorSubject.next(this.colorDefault);
  }

  ctn40ColorChange(color: string) {
    this.colorDefault.ctn40Color = color;
    this.colorSubject.next(this.colorDefault);
  }

  strokeColorChange(color: string) {
    this.colorDefault.stroke_color = color;
    this.colorSubject.next(this.colorDefault);
  }
}

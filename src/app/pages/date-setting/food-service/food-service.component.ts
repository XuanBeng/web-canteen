import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NzMessageService, NzModalRef, NzModalService } from 'ng-zorro-antd';
import { Observable } from 'rxjs';
import { Food, FoodState } from 'src/app/models/facade';
import { FoodFacadeService } from 'src/app/services/food-facade.service';

@Component({
  selector: 'app-food-service',
  templateUrl: './food-service.component.html',
  styleUrls: [
    './food-service.component.less',
    '../date-setting/date-setting.component.less'
  ]
})
export class FoodServiceComponent implements OnInit {
  searchTerm: FormControl;
  showButton = true;
  vm$: Observable<FoodState> = this.foodFacade.vm$;

  tplModal?: NzModalRef;
  tplModalButtonLoading = false;
  validateForm!: FormGroup;

  constructor(
    private foodFacade: FoodFacadeService,
    private modal: NzModalService,
    private fb: FormBuilder,
    private msg: NzMessageService
  ) {}

  ngOnInit(): void {
    const { criteria } = this.foodFacade.getStateSnapshot();
    this.searchTerm = this.foodFacade.buildSearchTermControl();
    this.searchTerm.patchValue(criteria, { emitEvent: false });
  }

  createTplModal(
    tplTitle: TemplateRef<{}>,
    tplContent: TemplateRef<{}>,
    mode: string,
    food?: Food
  ): void {
    console.log(mode, food);
    if (mode === 'add') {
      this.validateForm = this.fb.group({
        name: [null, [Validators.required]],
        type: [null, [Validators.required]],
        price: [null, [Validators.required]]
      });
    } else if (mode === 'update') {
      this.validateForm = this.fb.group({
        id: [food.id, [Validators.required]],
        name: [food.name, [Validators.required]],
        type: [food.type, [Validators.required]],
        price: [food.price, [Validators.required]]
      });
    }
    this.tplModal = this.modal.create({
      nzTitle: tplTitle,
      nzContent: tplContent,
      nzMaskClosable: false,
      nzClosable: false,
      nzComponentParams: {
        form: this.validateForm
      },
      nzOnOk: () => {
        console.log('Click ok', this.validateForm.value);
        if (mode === 'add') {
          this.foodFacade.post(this.validateForm.value).subscribe(res => {
            console.log('res', res);
            if (res.status === 0) {
              this.msg.success(res.msg);
            } else {
              this.msg.error(res.msg);
            }
          });
        } else if (mode === 'update') {
          this.foodFacade.put(this.validateForm.value);
          const res = this.foodFacade.getStateSnapshot().response;
          if (res.status === 0) {
            this.msg.success(res.msg);
          } else {
            this.msg.error(res.msg);
          }
        }
      }
    });
  }

  destroyTplModal() {
    this.tplModalButtonLoading = true;
    setTimeout(() => {
      this.tplModalButtonLoading = false;
      this.tplModal.destroy();
    }, 1000);
  }
}

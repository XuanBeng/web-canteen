import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { DataSettingService } from 'src/app/services/data-setting.service';

@Component({
  selector: 'app-seat',
  templateUrl: './seat.component.html',
  styleUrls: [
    './seat.component.less',
    '../date-setting/date-setting.component.less'
  ]
})
export class SeatComponent implements OnInit {
  seat$: Observable<any[]>;
  seatList: any[] = [];
  seatLsit$: Observable<any[]>;
  change$ = new BehaviorSubject<boolean>(false);

  searchString: string;

  globalEdit = false;
  addEidt = false;
  updateEdit = false;
  addEidt$ = new BehaviorSubject<boolean>(false);

  constructor(
    private dataSettingService: DataSettingService,
    private message: NzMessageService
  ) {}

  ngOnInit() {
    this.seat$ = this.change$.pipe(
      switchMap(() => this.dataSettingService.seatGet())
    );
    this.seatLsit$ = combineLatest([this.addEidt$, this.seat$]).pipe(
      map(([addEidt, seats]) => {
        const newSeats = seats.map(seat => {
          return {
            ...seat,
            edit: false
          };
        });
        const newseat = {
          number: '',
          people_no: 0,
          edit: true
        };
        if (addEidt === true) {
          return [newseat, ...newSeats];
        } else {
          this.seatList = newSeats;
          return newSeats;
        }
      })
    );
  }

  // 搜索
  search() {
    setTimeout(() => {
      // this.displayUnitList = this.unitList.filter(unit =>
      //   _searchFilter(this.searchString, unit, ['name'], 0)
      // );
    }, 0);
  }

  startEdit(data) {
    this.updateEdit = true;
    if (this.globalEdit === true) {
      console.log('已编辑其他数据');
    } else {
      data.edit = true;
      this.globalEdit = true;
    }
  }

  cancelEdit(data) {
    data.edit = false;
    this.globalEdit = false;
    if (this.addEidt === true) {
      this.addEidt = false;
      this.addEidt$.next(false);
    }
  }

  save(data) {
    console.log(data);
    data.edit = false;
    this.globalEdit = false;
    if (this.updateEdit === true) {
      // 调修改接口
      this.updateEdit = false;
    }
    if (this.addEidt === true) {
      // 调新增接口
      this.addEidt = false;
    }
  }

  add() {
    this.globalEdit = true;
    this.addEidt = true;
    this.addEidt$.next(true);
  }
}

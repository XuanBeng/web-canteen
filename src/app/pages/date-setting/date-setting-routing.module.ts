import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DateSettingComponent } from './date-setting/date-setting.component';
import { FoodServiceComponent } from './food-service/food-service.component';
import { FoodComponent } from './food/food.component';
import { SeatComponent } from './seat/seat.component';
import { UserComponent } from './user/user.component';

const routes: Routes = [
  {
    path: '',
    component: DateSettingComponent,
    children: [
      { path: '', redirectTo: 'food-service', pathMatch: 'full' },
      { path: 'food', component: FoodComponent },
      { path: 'seat', component: SeatComponent },
      { path: 'user', component: UserComponent },
      { path: 'food-service', component: FoodServiceComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DateSettingRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Unit } from 'src/app/models/data-setting';
import { User } from 'src/app/models/user';
import { DataSettingService } from 'src/app/services/data-setting.service';
import { _searchFilter } from 'src/app/util/function-lib';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: [
    './user.component.less',
    '../date-setting/date-setting.component.less'
  ]
})
export class UserComponent implements OnInit {
  userList: User[]; // 从后台获取的ic卡数据list
  displayUserList: User[]; // 用户显示的数据list
  user$: Observable<any[]>;
  unitList: Unit[];

  editCache = []; // 编辑状态下的显示数据list
  searchString = ''; // 搜索框中的数值
  editStatus = false; // 当前是否处于新增/编辑状态
  updateMode: 'edit' | 'add';
  // options = []; // 模态框下拉选择上级分类
  // optionValues = []; // 用于绑定option的值
  selectedValue: string;
  // parentIdList: string[];
  // parentNameList: string[];
  deleteConfirmModal: NzModalRef;
  loading = false;

  user: User;

  constructor(private dataSettingService: DataSettingService) {}

  ngOnInit() {
    this.user$ = this.dataSettingService.userGet().pipe(
      tap(res => {
        console.log(res);
        this.displayUserList = res;
      })
    );
  }

  // 搜索
  search() {
    setTimeout(() => {
      this.displayUserList = this.userList.filter(unit =>
        _searchFilter(this.searchString, unit, ['username', 'name'], 0)
      );
    }, 0);
  }
}

import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { DataSettingService } from 'src/app/services/data-setting.service';

@Component({
  selector: 'app-food',
  templateUrl: './food.component.html',
  styleUrls: [
    './food.component.less',
    '../date-setting/date-setting.component.less'
  ]
})
export class FoodComponent implements OnInit {
  food$: Observable<any[]>;
  foodList: any[] = [];
  foodLsit$: Observable<any[]>;
  change$ = new BehaviorSubject<boolean>(false);

  searchString: string;

  globalEdit = false;
  addEidt = false;
  updateEdit = false;
  addEidt$ = new BehaviorSubject<boolean>(false);

  constructor(
    private dataSettingService: DataSettingService,
    private message: NzMessageService
  ) {}

  ngOnInit() {
    this.food$ = this.change$.pipe(
      switchMap(() => this.dataSettingService.foodGet())
    );
    this.foodLsit$ = combineLatest([this.addEidt$, this.food$]).pipe(
      map(([addEidt, foods]) => {
        const newFoods = foods.map(food => {
          return {
            ...food,
            edit: false
          };
        });
        const newfood = {
          name: '',
          type: '',
          price: '',
          edit: true
        };
        if (addEidt === true) {
          return [newfood, ...newFoods];
        } else {
          console.log(newFoods);
          this.foodList = newFoods;
          return newFoods;
        }
      })
    );
  }

  // 搜索
  search() {
    setTimeout(() => {
      // this.displayUnitList = this.unitList.filter(unit =>
      //   _searchFilter(this.searchString, unit, ['name'], 0)
      // );
    }, 0);
  }

  startEdit(data) {
    this.updateEdit = true;
    if (this.globalEdit === true) {
      console.log('已编辑其他数据');
    } else {
      data.edit = true;
      this.globalEdit = true;
    }
  }

  cancelEdit(data) {
    console.log(data);
    data.edit = false;
    this.globalEdit = false;
    if (this.addEidt === true) {
      this.addEidt = false;
      this.addEidt$.next(false);
    }
    this.change$.next(true);
  }

  save(data) {
    // console.log(data);
    data.edit = false;
    this.globalEdit = false;
    if (this.updateEdit === true) {
      // 调修改接口
      this.dataSettingService.foodPut(data).subscribe(res => {
        if (res.status === 0) {
          this.change$.next(true);
        }
      });
      this.updateEdit = false;
    }
    if (this.addEidt === true) {
      // 调新增接口
      this.dataSettingService.foodPost(data).subscribe(res => {
        if (res.status === 0) {
          this.change$.next(true);
          this.addEidt$.next(false);
        }
      });
      this.addEidt = false;
    }
  }

  add() {
    this.globalEdit = true;
    this.addEidt = true;
    this.addEidt$.next(true);
  }

  cancel() {}
}

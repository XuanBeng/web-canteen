import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  NzButtonModule,
  NzCardModule,
  NzDividerModule,
  NzFormModule,
  NzInputModule,
  NzMessageModule,
  NzModalModule,
  NzPopconfirmModule,
  NzTableModule,
} from 'ng-zorro-antd';

import { TestModule } from '../test/test.module';
import { DateSettingRoutingModule } from './date-setting-routing.module';
import { DateSettingComponent } from './date-setting/date-setting.component';
import { FoodServiceComponent } from './food-service/food-service.component';
import { FoodComponent } from './food/food.component';
import { SeatComponent } from './seat/seat.component';
import { UserComponent } from './user/user.component';

@NgModule({
  declarations: [
    DateSettingComponent,
    UserComponent,
    FoodComponent,
    SeatComponent,
    FoodServiceComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TestModule,
    NzCardModule,
    NzTableModule,
    NzDividerModule,
    NzMessageModule,
    NzButtonModule,
    NzPopconfirmModule,
    NzInputModule,
    NzModalModule,
    NzFormModule,
    DateSettingRoutingModule
  ]
})
export class DateSettingModule {}

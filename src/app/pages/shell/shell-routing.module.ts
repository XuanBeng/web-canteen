import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ShellComponent } from './shell.component';

const routes: Routes = [
  {
    path: '',
    component: ShellComponent,
    children: [
      { path: '', redirectTo: 'date-setting', pathMatch: 'full' },
      {
        path: 'date-setting',
        loadChildren: () =>
          import('../date-setting/date-setting.module').then(
            m => m.DateSettingModule
          )
      },
      {
        path: 'canteen',
        loadChildren: () =>
          import('../canteen/canteen.module').then(m => m.CanteenModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShellRoutingModule {}

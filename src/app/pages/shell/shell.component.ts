import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.less']
})
export class ShellComponent implements OnInit {
  isCollapsed = false;
  userName: string;
  type: any;

  constructor(private loginService: LoginService) {}

  ngOnInit() {
    this.getUserInfo();
  }

  getUserInfo() {}

  logout() {
    this.loginService.logout();
  }
}

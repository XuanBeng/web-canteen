import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzDropDownModule, NzLayoutModule, NzMenuModule } from 'ng-zorro-antd';

import { ShellRoutingModule } from './shell-routing.module';
import { ShellComponent } from './shell.component';

@NgModule({
  imports: [
    CommonModule,
    NzLayoutModule,
    NzMenuModule,
    NzDropDownModule,
    FormsModule,
    ReactiveFormsModule,
    ShellRoutingModule
  ],
  declarations: [ShellComponent]
})
export class ShellModule {}

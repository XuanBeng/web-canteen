import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';
import { Unit } from 'src/app/models/data-setting';
import { User } from 'src/app/models/user';
import { DataSettingService } from 'src/app/services/data-setting.service';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
  validateRegisterForm: FormGroup;
  validateLoginForm: FormGroup;

  isVisible = false;
  isOkLoading = false;

  unitList: Unit[];
  select: string;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private loginService: LoginService,
    private message: NzMessageService,
    private dataSettingService: DataSettingService
  ) {}

  ngOnInit(): void {
    // 登录表单
    this.validateLoginForm = this.fb.group({
      userName: [null, [Validators.required]],
      password: [null, [Validators.required]],
      // type: [null, [Validators.required]],
      remember: [true]
    });

    // 注册表单
    this.validateRegisterForm = this.fb.group({
      student_no: [null, [Validators.required]],
      password: [null, [Validators.required]],
      checkPassword: [null, [Validators.required, this.confirmationValidator]],
      name: [null, [Validators.required]],
      phoneNumberPrefix: ['+86'],
      phoneNumber: [null, [Validators.required]],
      unit: [null, [Validators.required]],
      agree: [false]
    });
    this.loadData();
  }

  /**
   * 加载数据
   */
  loadData() {}

  /**
   * 用户登录
   */
  submitLoginForm(): void {
    // tslint:disable-next-line: forin
    for (const i in this.validateLoginForm.controls) {
      this.validateLoginForm.controls[i].markAsDirty();
      this.validateLoginForm.controls[i].updateValueAndValidity();
    }

    if (this.validateLoginForm.valid) {
      this.loginService
        .login(
          this.validateLoginForm.value.userName,
          this.validateLoginForm.value.password
        )
        .subscribe(res => {
          if (res.status === 0) {
            this.message.success(res.msg);
            this.router.navigate(['shell']);
          } else {
            console.log(res.msg);
            // tslint:disable-next-line: no-string-literal
            this.message.error(res.msg['detail'][0]);
          }
        });
    }
  }

  /**
   * 用户注册
   */
  submitRegisterForm(): void {
    // tslint:disable-next-line: forin
    for (const i in this.validateRegisterForm.controls) {
      this.validateRegisterForm.controls[i].markAsDirty();
      this.validateRegisterForm.controls[i].updateValueAndValidity();
    }
    this.isOkLoading = true;
    // setTimeout(() => {
    //   this.isVisible = false;
    //   this.isOkLoading = false;
    // }, 3000);
    if (this.validateRegisterForm.value.agree === false) {
      this.message.error('请确认信息');
    } else {
      const user: User = {
        username: this.validateRegisterForm.value.student_no,
        password: this.validateRegisterForm.value.password,
        name: this.validateRegisterForm.value.name,
        mobile:
          this.validateRegisterForm.value.phoneNumberPrefix +
          this.validateRegisterForm.value.phoneNumber,
        unit: this.validateRegisterForm.value.unit,
        student_no: this.validateRegisterForm.value.student_no,
        type: '0'
      };
      console.log(user);
      this.loginService.register(user).subscribe(res => {
        if (res.status === 0) {
          this.message.success('注册成功!');
          this.isVisible = false;
          this.isOkLoading = false;
          console.log('res', res);
        }
      });
    }
  }

  updateConfirmValidator(): void {
    /** wait for refresh value */
    Promise.resolve().then(() =>
      this.validateRegisterForm.controls.checkPassword.updateValueAndValidity()
    );
  }
  confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { required: true };
    } else if (
      control.value !== this.validateRegisterForm.controls.password.value
    ) {
      return { confirm: true, error: true };
    }
    return {};
    // tslint:disable-next-line: semicolon
  };
  getCaptcha(e: MouseEvent): void {
    e.preventDefault();
  }
}

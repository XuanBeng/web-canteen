import { Component, OnInit } from '@angular/core';
import { NzMessageService, NzModalService } from 'ng-zorro-antd';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { CanteenService } from 'src/app/services/canteen.service';
import { DataSettingService } from 'src/app/services/data-setting.service';

import { FoodOrderedComponent } from '../food-ordered/food-ordered.component';
import { SelectFoodComponent } from '../select-food/select-food.component';

@Component({
  selector: 'app-select-seat',
  templateUrl: './select-seat.component.html',
  styleUrls: ['./select-seat.component.less']
})
export class SelectSeatComponent implements OnInit {
  seats$: Observable<any[]>;
  tzs$: Observable<any[]>;
  tzs: any[];
  display$: Observable<any[]>;
  change$ = new BehaviorSubject<boolean>(false);

  constructor(
    private canteenService: CanteenService,
    private message: NzMessageService,
    private dataSettingService: DataSettingService,
    private modal: NzModalService
  ) {}

  ngOnInit() {
    this.seats$ = this.dataSettingService.seatGet().pipe();
    this.tzs$ = this.change$.pipe(
      switchMap(() =>
        this.canteenService.tzGet().pipe(tap(res => (this.tzs = res)))
      )
    );

    this.display$ = combineLatest([this.seats$, this.tzs$]).pipe(
      map(([seats, tzs]) => {
        console.log('tzs', tzs);
        const seatsMap = {};
        const tzsMap = {};
        seats.forEach(seat => {
          seatsMap[seat.id] = seat;
        });
        tzs.forEach(tz => {
          tzsMap[tz.seat] = tz;
        });
        const displayList = [];
        for (const id in seatsMap) {
          if (seatsMap.hasOwnProperty(id)) {
            const seat = seatsMap[id];
            const display = {
              seat,
              tzId: -1,
              seatPeoples: 0,
              seatStatus: 2,
              totalPrice: 0,
              tz_details: []
            };
            if (tzsMap.hasOwnProperty(id)) {
              const tz = tzsMap[id];
              display.seatStatus = +tz.status;
              display.totalPrice = tz.total_price;
              display.tz_details = tz.tz_details;
              display.tzId = tz.id;
            }
            displayList.push(display);
          }
        }
        console.log('displayList', displayList);
        return displayList;
      })
    );
  }

  order(seat) {
    const modal = this.modal.create({
      nzTitle: '点餐',
      nzContent: SelectFoodComponent,
      nzGetContainer: () => document.body,
      nzComponentParams: {
        seat
      },
      nzWidth: 600,
      nzOnOk: selectFoodComponent => {
        const orderList = selectFoodComponent.orderList;
        console.log(orderList);
        let totalPrice = 0;
        for (const order of orderList) {
          totalPrice += order.amount * order.price;
        }
        const tz = {
          seat: seat.id,
          status: '0',
          total_price: totalPrice
        };
        this.canteenService.tzPost(tz).subscribe(res => {
          console.log(res);
          if (res.status === 0) {
            const tzDetails = [];
            for (const order of orderList) {
              const tzDetail = {
                tz: res.data.id,
                food: order.id,
                amount: order.amount
              };
              tzDetails.push(tzDetail);
            }
            this.canteenService.tzDetailPost(tzDetails).subscribe(tzd => {
              if (tzd.status === 0) {
                this.message.success('下单成功');
                this.change$.next(true);
              } else {
                this.message.error('下单失败');
              }
            });
          } else {
            this.message.error('下单失败');
          }
        });
      }
    });
    const instance = modal.getContentComponent();
    modal.afterOpen.subscribe();
    // Return a result when closed
    modal.afterClose.subscribe();
  }

  foodOrdered(display) {
    const modal = this.modal.create({
      nzTitle: '已点菜单',
      nzContent: FoodOrderedComponent,
      nzGetContainer: () => document.body,
      nzComponentParams: {
        foodList: display.tz_details
      },
      nzWidth: 600
    });
  }

  pay(display) {
    // const tz = this.tzs.find(t => t.id === display.tzId);
    const postTz = {
      id: display.tzId,
      status: '1'
    };
    this.canteenService.tzPut(postTz).subscribe(res => {
      this.message.success('结账成功');
      this.change$.next(true);
    });
  }
}

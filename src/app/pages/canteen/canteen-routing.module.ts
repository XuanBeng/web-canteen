import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CanteenComponent } from './canteen/canteen.component';
import { SelectFoodComponent } from './select-food/select-food.component';
import { SelectSeatComponent } from './select-seat/select-seat.component';

const routes: Routes = [
  {
    path: '',
    component: CanteenComponent,
    children: [
      { path: '', redirectTo: 'select-seat', pathMatch: 'full' },
      { path: 'select-seat', component: SelectSeatComponent },
      { path: 'select-food', component: SelectFoodComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CanteenRoutingModule {}

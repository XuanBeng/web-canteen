import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {
  NzBadgeModule,
  NzButtonModule,
  NzCardModule,
  NzGridModule,
  NzInputModule,
  NzInputNumberModule,
  NzListModule,
  NzMessageModule,
  NzModalModule,
  NzTableModule,
} from 'ng-zorro-antd';

import { CanteenRoutingModule } from './canteen-routing.module';
import { CanteenComponent } from './canteen/canteen.component';
import { FoodOrderedComponent } from './food-ordered/food-ordered.component';
import { SelectFoodComponent } from './select-food/select-food.component';
import { SelectSeatComponent } from './select-seat/select-seat.component';

@NgModule({
  declarations: [
    CanteenComponent,
    SelectSeatComponent,
    SelectFoodComponent,
    FoodOrderedComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NzCardModule,
    NzInputModule,
    NzInputNumberModule,
    NzButtonModule,
    NzModalModule,
    NzListModule,
    NzTableModule,
    NzBadgeModule,
    NzGridModule,
    NzMessageModule,
    CanteenRoutingModule
  ]
})
export class CanteenModule {}

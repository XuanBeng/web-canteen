import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DataSettingService } from 'src/app/services/data-setting.service';

@Component({
  selector: 'app-select-food',
  templateUrl: './select-food.component.html',
  styleUrls: ['./select-food.component.less']
})
export class SelectFoodComponent implements OnInit {
  @Input() seat: any;
  foods$: Observable<any[]>;

  constructor(private dataSettingService: DataSettingService) {}

  orderList: any[] = [];

  ngOnInit() {
    this.foods$ = this.dataSettingService.foodGet();
  }

  orderFood(food) {
    let ifExist = true;
    for (const order of this.orderList) {
      if (order.id === food.id) {
        order.amount = food.amount;
        ifExist = true;
        break;
      }
      ifExist = false;
    }
    if (this.orderList.length === 0) {
      ifExist = false;
    }
    if (!ifExist) {
      this.orderList.push(food);
    }
  }
}

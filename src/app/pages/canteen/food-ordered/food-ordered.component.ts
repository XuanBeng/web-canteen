import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-food-ordered',
  templateUrl: './food-ordered.component.html',
  styleUrls: ['./food-ordered.component.less']
})
export class FoodOrderedComponent implements OnInit {
  @Input() foodList: any[];
  constructor() {}

  ngOnInit(): void {
    console.log(this.foodList);
  }
}

import { environment } from 'src/environments/environment';

// export const baseUrl = 'http://127.0.0.1:8000/health/api/v1/';

const baseUrl = environment.baseUrl;

export const loginUrl = baseUrl + 'login/';
export const userUrl = baseUrl + 'user_detail/';
export const foodUrl = baseUrl + 'food/';
export const seatUrl = baseUrl + 'seat/';
export const tzUrl = baseUrl + 'tz/';
export const tzDetailUrl = baseUrl + 'tz_detail/';
